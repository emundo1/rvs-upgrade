import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const features = [
  {
    title: 'ILM Upgrade Process',
    imageUrl: 'img/undraw_docusaurus_react.svg',
    description: (
      <>
        This document will teach you how to upgrade ILM from version 11.2 to 12.4
      </>
    ),
  },
  {
    title: 'IRC Documentation',
    imageUrl: 'img/undraw_docusaurus_tree.svg',
    description: (
        <>
          This document will teach you all about Integrity Requirmnts Connecter and how to use our automations
        </>
      ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={clsx(styles.feature)} class="feature-item">
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3 className='text--center'>{title}</h3>
      <p className='text--center'>{description}</p>
    </div>
  );
}

export default function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Integrity Documentation and Process\.<head />">
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <img class="home_header_img" src="img/PTC_Integrity_logo.png"></img> 
          <h1 className="hero__title offgray_text">{siteConfig.title}</h1>
          <p className="hero__subtitle offgray_text">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={clsx(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/')}>
              Get Started
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container coolbox">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}
