module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Upgrade Guide',
      items: [
        'index',
        'rvs-upgrade/db-refresh',
        'rvs-upgrade/upgrade-rvs',
        'rvs-upgrade/configure-rvs',
        'rvs-upgrade/testing-rvs',
        'rvs-upgrade/definitions'
      ],
    },
    {
      type: 'category',
      label: 'IRC Install',
      items: [
      'irc/irc-install'
      ],
    },
    {
      type: 'category',
      label: 'About Docusaurus',
      items: [
      'docusaurus-info/intro'
      ],
    },
  ],
};
