/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'RV&S Documentation',
  tagline: 'Documents and Procedures for Windchill RV&S',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/rvs-upgrade/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/light-logo.ico',
  organizationName: 'IQNOX', // Usually your GitHub org/user name.
  projectName: 'rvs-upgrade', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'RV&S Documentation',
      logo: {
        alt: 'RV&S-Logo',
        src: 'img/light-logo.ico',
        srcDark: 'img/dark-logo.ico'
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs/$',
          label: 'Upgrade Guide',
          position: 'left',
        },
        {
          to: 'docs/irc-install',
          activeBasePath: 'docs/irc-install/$',
          label: 'IRC Documents',
          position: 'left',
        },
        {
          to: 'docs/docusaurus-info',
          activeBasePath: 'docs/docusaurus-info/$',
          label: 'About Docusaurus',
          position: 'left',
        },
        {
          to: 'https://gitlab.com/emundo1/rvs-upgrade',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      copyright: `Copyright © ${new Date().getFullYear()} IQNOX`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/emundo1/rvs-upgrade/-/blob/master/',
            showLastUpdateAuthor: true,
            showLastUpdateTime: true,

        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    require.resolve('docusaurus-lunr-search')
  ],
};
