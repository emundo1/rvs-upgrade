---
title: Upgrade Guide
slug: /
---

## Intro
Hello and welcome to the definitive guide on how to upgrade ILM version 11.2 (ILM 11.2) to RV&S version 12.4 (RV&S 12.4). In this guide, we will go over all the steps needed to upgrade ILM 11.2 to 12.4. We will also document where to find any internal/external resources useful during your upgrade. We will outline where to find common configuration files that will need to be updated during the upgrade, along with site-specific pitfalls to look out for.

## Process 
The RV&S upgrade processes consist of multiple tasks that should be completed in phases. These Phases are as follows:
1. Perform a Database Refresh.
2. Upgrade The RV&S Server.
3. Configure RV&S.
4. Test The RV&S Server.

## Database Refresh
The database refresh step is the first thing that should be done when upgrading to RV&S 12.4. This process should be managed by a DBA. This task will ensure that the data on all servers is exactly the same across all [servers in a chain](/docs/Definitions#server-chain). This process consists of taking the data from a source database and copying it onto a separate database. After the database refresh is completed by the DBA team, the server maintainers will copy over server files like scripts, reports, public_html data. The server will then be renamed to match the hostname of the server and be brought back online. 
:::caution
RV&S 12.4 uses Oracle's 19c databases, while ILM 11.2 uses Oracle's 12c Databases. This should be taken into account when migrating the databases.
:::
:::caution
When creating the database backup, it is **important** to coordinate with the server maintainers. If a difference is detected between any of the databases, the server will refuse to start. If a change is made after the backup is taken, the backup will not reflect the exact state of the source database. Letting the server maintainer know when you will start your backup will signal to stop all development on all chains being refreshed.
:::
### Upgrade to RV&S

### Configure RV&S

### Test RV&S
