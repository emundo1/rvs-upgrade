---
title: Upgrade to RV&S
slug: /Upgrade-RVS
sidebar_label: Upgrade to RV&S
---
### Checklist Purpose

The numbered checklist that follows serves as a detailed guide for the upgrade from ILM 11.2 to RVS 12.4. It is suggested that prior to starting an upgrade, the user should print a hard copy of this document and cross off actions as they are completed. The intent is that we will have several practice upgrades prior to the final upgrade. After each practice upgrade, update this document based on lessons learned.

## Prep-Upgrade 
#### (~4 hours)
***

### Oracle Database 

1. Before upgrading, stop the production ILM 11.2 Server using the following command from the bin directory of the Integrity server:
    
    ```sh
    cd /opt/pdm/Integrity/bin
    dzdo ./mksis stop
    ```
    :::note

    `/opt/pdm/Integrity/` might not be the install directory for integrity if this is the case replace the `cd` command with the correct install directory

    :::

    :::info

    It is acceptable to leave the production server running when the goal is to restore to a non-staged Integrity Server. A non-staged Integrity Server might be a sandbox server or the training server. These servers are for testing purposes. For final upgrade scenarios, we want to insure that there is no data loss due to continued update to the production server after the backup for final cutover.

    :::

1. The DBA makes a full backup of the production Oracle 11.2 database. This will be used both as a backup for recovery purposes and as a starting point for an upgrade to servers that are not currently staged including the training server or one of the sandbox servers. For the production Integrity Server, the database is up-graded in place.

2. For final go-live production upgrade: If this is a staged server configuration, we will also make a full backup of the last server in the staging sequence (production server) after the upgrade of that server to 12.2. We will restore the backup of the 12.2 production server to lower stage servers. This is required because creation of a locked staging configuration requires identical Oracle databases.

3. If the upgrade target is not the production Integrity Server (ilm.devsite.com:443) and is not part of a staging configuration, the DBA should copy the full backup of the Oracle 11.2 database to target server’s database location. For the production Integrity Server, the database is up-graded in place.

    :::caution

    Before leaving this section on the Oracle database, it is important to insure that we have a full backup of the production 11.2 database. Restore the backup of the production 11.2 database only as a starting point to non-staged server upgrades (e.g. training or sandbox servers). If the servers are participants in a staging configuration, the lower stages are instead, updated with a copy of the database from the production server in the staging chain AFTER that server is upgraded to Integrity Server 12.2.

    :::



### Integrity Server

1. Rename the current Integrity Server from Integrity to IntegrityOLD For example:
    
    ```sh
    cd /opt/pdm
    dzdo mv Integrity Integrity-11-2
    ```

1. Obtain the latest installers for RV&S Server 12.4 and RV&S Client 12.4 from the PTC software downloads site. Place the installer in the /tmp/staging directory. https://support.ptc.com/appserver/auth/it/esd/product.jsp?prodFamily=MIG

    - Set the installer to executable: 
        
        ```sh
        cd /tmp/staging
        dzdo chmod +x mksserver.bin
        ```

2. Download the latest CPS (Critical Service Patch) from the PTC product web site. You will use this in a subsequent step to complete the upgrade process. https://support.ptc.com/appserver/auth/it/esd/product.jsp?prodFamily=MIG

    + Unzip the downloaded file to access two inner files. One of the files is a zip file similar in name to:

      `HF-S170000000-903.zip`

    + Set this file for execution and then deletion by the ILM PatchServer.
        ```sh
        dzdo chmod 777 HF-S170000000-903.zip
        ```
    + Place the HF file (example above) in the /tmp/staging folder on each target upgrade servers. The file is used is a later stage of the upgrade.

1. Create an installation properties file for execution of the upgrade. The file name should be `mksserver.properties`. This file should be placed in the directory where the `mksserver.sh` file is locate. Most of the contents can be left unchanged with exception of the following:

    | Property                             | Description                                                                                                                                                                                                                         |
    |--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
    | INSTALLER_UI                         | Property INSTALLER_UI defines install mode - "silent" or "gui"                                                                                                                                                                      |
    | MKS_PRODUCT_INSTALLATION             | Set to true if user want to install new Windchill RV&S Server                                                                                                                                                                       |
    | MKS_MAINT_INSTALLATION               | Set to true if user want to install Maintanence Patch, otherwise false                                                                                                                                                              |
    | MKS_PATCHSET_INSTALLATION            | Set to true if user want to install patchset, otherwise false                                                                                                                                                                       |
    | MKS_LICENSE_AGREEMENT                | If you accept the terms of the license agreement change the value to true                                                                                                                                                           |
    | MKS_JAVA_EXECUTABLE                  | Java Virtual Machine to be used for the installed product.                                                                                                                                                                          |
    | USER_INSTALL_DIR                     | Property USER_INSTALL_DIR used to specify where to install Windchill RV&S Server                                                                                                                                                    |
    | MKS_UPGRADE                          | Set MKS_UPGRADE=true if this is installation is part of an upgrade                                                                                                                                                                  |
    | MKS_EXISTING_INSTALL_DIR             | Existing Server Installation Directory If MKS_UPGRADE=true then set this property to the installation location                                                                                                                      |
    | MKS_LICENSE_FILE                     | Windchill RV&S Server license file location                                                                                                                                                                                         |
    | MKS_DATABASE_TYPE                    | Specify a database to use with the Windchill RV&S Server                                                                                                                                                                            |
    | MKS_DATABASE_VERSION                 | Supported Database Version- For Oracle, Oracle_19c is supported. For SQL, MSSQL_2016_SP2+/MSSQL_2017/MSSQL_2019 are supported.                                                                                                      |
    | MKS_DATABASE_HOST_NAME               | Database server host name                                                                                                                                                                                                           |
    | MKS_DATABASE_PORT                    | Default for Oracle: 1521                                                                                                                                                                                                            |
    | MKS_DATABASE_NAME                    | Database name (for Oracle the Database Service Name)                                                                                                                                                                                |
    | MKS_DATABASE_LOGIN_NAME              | Database login name                                                                                                                                                                                                                 |
    | MKS_DATABASE_LOGIN_PASSWORD          | Database password                                                                                                                                                                                                                   |
    | MKS_INSTANCE_NAME                    | Database instance name (only used with Microsoft SQL Server)                                                                                                                                                                        |
    | MKS_MIGRATE_EXISTING_DATA            | If an existing set of Windchill RV&S Server data is detected during installation, do you want to migrate this data?                                                                                                                 |
    | MKS_CASE_INSENSITIVE                 | Do you want to create case-sensitive database tables?                                                                                                                                                                               |
    | MKS_NOTIFY_MAIL_SERVER               | The Windchill RV&S Server has the capability to send emails using triggers. The Mail Server and Mail Server properties must be correctly configured in order to receive emails. The Email To property can be a single email address |
    | MKS_NOTIFY_MAIL_SERVER_PORT          | The Windchill RV&S Server has the capability to send emails using triggers. The Mail Server and Mail Server properties must be correctly configured in order to receive emails. The Email To property can be a single email address |
    | MKS_NOTIFY_EMAIL_TO                  | The Windchill RV&S Server has the capability to send emails using triggers. The Mail Server and Mail Server properties must be correctly configured in order to receive emails. The Email To property can be a single email address |
    | MKS_PROXY_SERVER                     | The Windchill RV&S Server has the capability to run as a Full Server or as a Proxy                                                                                                                                                  |
    | MKS_WORKFLOWS_AND_DOCUMENTS_ENABLED  | Workflows and Documents capabilities of the Windchill RV&S Server.                                                                                                                                                                  |
    | MKS_CONFIGURATION_MANAGEMENT_ENABLED | Configuration Management capabilities of the Windchill RV&S Server.                                                                                                                                                                 |
    | MKS_PRODUCT_LANGUAGE                 | Choose one language to enable using the two-letter ISO-639 code and optional                                                                                                                                                        |

1. See Appendix A for example `mksserver.properties` files.
2. Place the `mksserver.properties` file in the location where you intend to execute the upgrade on the target Linux server (e.g. `/tmp/staging`)
3. Note the LDAP credentials in `security.properties` for the server that you are upgrading. It is important to note these because the production server may use different LDAP servers with different credentials than those on development, QA, and training servers.
4. Place a copy of the Integrity 11.2 installation executable (mksserver.bin) in the location where you intend to execute the upgrade on the target Linux serve (e.g. /home/wtuser).

### Pre-Positioning of files

Copy the following member of the data directory of the production 11.2
server to the corresponding paths on the target server. The target
server name should now be IntegrityOLD.

- /opt/pdm/Integrity/data/public_html
- /opt/pdm/Integrity/data/reports
- /opt/pdm/Integrity/data/triggers

1. To achive this you can run the following commands on the destination server. 

        mkdir datadir
        rsync -rav -e ssh user@ilm.devsite.com:/opt/pdm/Integrity/data/{public_html,reports,triggers} /home/user/datadir/

    - `mkdir datadir` Will create a directory called datadir in this case this was run in our users home directory `/home/user/datadir/`
    - `rsync -rav -e ssh user@ilm.devsite.com:/opt/pdm/Integrity/data/{public_html,reports,triggers} /home/user/datadir/` will download our files from the source server to the destination server.
        + `rsync` will sync files from one server to another
        + `-rav -e ssh` are command line arguments that will augment how our sync is ran.
            * `r` will download files recessively.
            * `a` will run the sync in archival mode to not waste time downloading files we already have.
            * `v` will print out verbose logs. 
            * `e ssh` will use SSH as its transfer protocol.
        + `user@ilm.devsite.com:/opt/pdm/Integrity/data/{public_html,reports,triggers}` will download `public_html`, `reports`, `triggers` as the `user` user from `ilm.devsite.com`
            * make sure to swap out `user` for your unix user and `ilm.devsite.com` for the correct source server. 
        + `/home/user/datadir/` will save the files on the destination servers at the directory we created in the last command. 

1. If needed, edit the gateway-tool-configuration.xml file in the
    staging folder to change all instance of ilm.devsite.com:443 to the new
    hostname and port combination (e.g. replace ilm.devsite.com:443 with
    ilm2.devsite.com:443. 

    :::note

    always use fully qualified names with Integrity

    ::: 

    :::note
    
    This is not needed for `ilm-dev.devsite.com`, `ilm-qa.devsite.com`, and `ilm.devsite.com`

    :::

1. Copy the server certificate into the staged “data” directory. Place
    it in /data/tls in the /tmp/staging/data file structure.
    
    :::note
    
    The dev, QA, and Production servers will need new server certificates due to upcoming expirations in the first week of November.

    :::

1. Copy the server certificate to certificate.p12
    ```
    cd /opt/pdm/ILMServer11/data/tls
    cp xxxxxxx.devsite.com.p12 certificate.p12
    ```
    - **where xxxxxx is replaced with the server name **

1. Place the server salt file (**encryption.properties**) in /tmp/staging/ (Only if server log on startup shows an encryption error – otherwise existing file should be correct).

2. Place the server SSL certificate in **/tmp/staging/ServerCerts**

3. Place the JRE Certificates for inter server communication in
    **/tmp/staging/JRECerts**

4. In the IntegrityOLD server directory files, turn off password encryption and enter the passwords for Oracle, LDAP lookup, private key for server certificate, and API Session in clear text (Encryption can be turned back on after the server upgrade is complete)

## Execute the Integrity Server Upgrade 
#### (~3 hours)
***


1. If the install directory of the server that you are upgrading is `Integrity`, rename it so that there will not be conflicts on the install since the installation script will use that directory for the new server. The rename should be to `Integrity-<version-number>`.

2.  After verifying that the DBA has executed the steps in the Oracle Database section above, execute the upgrade from the target server. Do this by changing to the directory described above where you have already placed the mksserver.bin and mksserver.properties files. This should be **/tmp/staging**
    
    ```sh
    cd /tmp/staging
    ```

    :::note 

    The execution of the next command (mksserver.bin) will be silent and may take 10 to 15 minutes to complete. Track progress by monitoring the dbinstall.log file in the log directory.

    :::

    ```sh
    dzdo ./mksserver.bin
    ```

    :::note

    The above syntax requires that the mksserver.properties file be in the same directory as the mksserver.bin file.

    :::

1.  Follow the prompts to install the Integrity 12.2 Server. 

    :::caution

    DO NOT start the new Integrity Server yet.

    :::

2.  Change to the bin directory of the new Integrity 11.2 Server
    
    ```sh
    cd /opt/pdm/Integrity/bin
    ```

1.  From the bin directory of the new Integrity 11.2 server, execute the migrateServerConfig option of isutil to synchronize file system artifacts in the new Integrity server with those in the old Integrity server. Use the path to the old server as an argument as shown below.

    ```sh
    ./isutil –c migrateServerConfig /opt/pdm/IntegrityOLD
    ```

    :::note

    Although the above command has limited interaction and is long running. Patience is required.

    :::

1. Only if encryption errrors are found in the server log at startup: If this is a new server, copy the encryption.properties file from /tmp/staging/encryption.properties to the /config/properties directory of the new server. This “server salt” file is a required key for encryption/decryption of passwords stored in an encrypted form in the Oracle database.

    ```sh
    cd /opt/pdm/Integrity/config/properties
    dzdo cp /tmp/staging/encryption.properties .
    ```

1. Perform a check of all of the files in the new install directory. You should check that the config/properties files are correct. It is important to note that production servers use different LDAP server with different credentials than those on `DEV` and `QA` servers.

2. In a prior step, files from the data directory of the production server were copied to the IntegrityOLD directory structure. Copy all of the files in data directory of the production server to the data directory of the target server.
    1. Assuming that RV&S was installed at `/opt/pdm/Integrity` remove the reports, public_html, and triggers directories.
    
        ```sh 
        dzdo rm -rfv /opt/pdm/Integrity/data/{public_html,triggers,reports}
        ```

3. Next, replace the deleted directories with the latest from the production server. Assuming the data files where saved to your home directory at `datadir`. 

    :::caution 

    Remeber to change the user directory to match your user.

    :::

    ```
    dzdo cp –a /home/user/datadir/{reports,public_html,triggers} /opt/pdm/Integrity/data/
    ```

1. In the `/opt/pdm/Integrity/config/client/IntegritySiteClient.rc` file change the connections policy from specific connection to allow All Connections. This is for Gateway Custom Button functions. Ensure that authentication is required.
2. Install required SSL certificates. This is only required if files are not copied from the old server.
    -   Copy the contents of /tmp/staging/JRECerts to /jre/lib/security
    -   Verify that the /data/tls directory has the certificate for the server. If not, copy it from c:/tmp/ServerCerts
3. Enable SSL for LDAP. This is only required if files are not copied from old server
    -   In security.properties change the LDAP port from 389 to 636
        -   ldap.port=389 -> 636
    -   In security.properties change the LDAP server from devsite.com to ldappw.devsite.com
        -   ldap.host=devsite.com -> ldappw.devsite.com
    -   In security.properties insert a new key value pair ldap.ssl=true
        -   ldap.ssl=true
    -   Import a new jssecacerts file to the Integrity Server
       
        ```sh        
        cd /opt/pdm/java/jre/lib/security/
        dzdo mv jssecacerts jssecacerts.bak
        dzdo cp /mnt/plm/wcdev/certs/jssecacerts .
        ```

1. If there is a license failure on server startup, the license copy may have failed on the installation. Copy the server license file to the new directory. The install process used the license file stored in the IntegrityOLD/data/license folder. Now that the new Integrity server directories exist in the file system, we can copy the license from the older server to the new server. In the new server install location:
    
    ```sh
    cd /opt/pdm/Integrity/data/license
    cp –a /opt/pdm/IntegrityOLD/data/license/license.dat .
    ```

  Edit the is.properties file to point to the license at the new location `/opt/pdm/ILMServer11/data/license`. (You will need to copy the file to a place where you can edit it and then copy it back to /opt/pdm/ILMServer11/config/properties).

1. Start the Integrity Server (you need to use dzdo or the server will not start).

    ```sh
    cd /opt/pdm/ILMServer11/bin
    dzdo./mksis start
    ```

### Upgrade Integrity Client

The Integrity Client is a full uninstall/reinstall, but there are some
tasks that need to be performed after the install of the new RV&S
Client 12.4 is completed. After installing the 12.4 Client values will
need to set in two files in the client’s bin directory.

1.  In the bin client directory, find the file `IntegrityClient.lax` and
    make the following changes to increase the maximum java heap size
    from the default to the maximum recommended by Integrity R&D. In the
    key: `lax.nl.java.option.additional`, change the value for `–Xmx` to
    `Xmx1200m`

2.  In the client bin directory, find the file `Gateway.lax` and make the
    following change to allow the execution of the Gateway jar file. For
    the key lax.class.path=, append the following to the path:

    ```
    c:\PTC\custom\GatewayDriver\IntegrityCustomGateway.jar
    ```

## Failure/Rollback Plan
***

In the event of a failure, it will be necessary to roll back to the 11.2 Integrity Server.



## Appendix A
***

**Sample mksserver.properties files** 

This is an example of a `mksserver.properties` file. User names and passwords have been stripped. 

```yaml
# Windchill RV&S Server silent installer properties file.
# To use it, run installer with "-f mksserver.properties"
#
# To change the locale that is used for the product,
# use the MKS_PRODUCT_LANGUAGE property.
#
# The -l option controls the language of the silent installer using the 
# two-letter ISO-639 code and optional ISO-3166 country code.
# For example:
#      English = en
#          Japanese = ja
#          German = de
#          Chinese Simplified = zh_CN
#          Chinese Traditional = zh_TW
#          Korean = ko
#          French = fr
#
# Please note: This file must be updated to suit your environment.  A silent
# installation may not work without modifying this file first.

#
# Property INSTALLER_UI defines install mode - "silent" or "gui". 
# In silent mode you are not asked about any parameters - all of them are taken
# from this file.
#
INSTALLER_UI=silent

# Set to true if user want to install new Windchill RV&S Server
# Set to true if user want to install both patchset & Windchill RV&S Server
# Set to false if user want to install only patchset on existing Windchill RV&S Server
MKS_PRODUCT_INSTALLATION=true

# Set to true if user want to install Maintanence Patch, otherwise false
MKS_MAINT_INSTALLATION=false

# Set to true if user want to install patchset, otherwise false
MKS_PATCHSET_INSTALLATION=false

# If "MKS_PATCHSET_INSTALLATION" is true then this property is mandatory.
# Property used to input patchset file which must be a zip file starting with 'S'
# D:\\Users\\amhaske\\S190000001-00.zip

# MKS_SERVICE_PACK=

# If "MKS_PATCHSET_INSTALLATION" is true and "MKS_PRODUCT_INSTALLATION" is false then this property is mandatory.
# Property used to locate existing ILM server to install maintenance/patchset over it
# Examples:
# "C:\\Windchill\\RVSServer"
# "/usr/local/Windchill/RVSServer"

# MKS_EXISTING_INSTALL_DIR_FOR_PATCH=

# License Agreement
# You must read the license agreement and indicate your acceptance before you
# continue with the installation.
# A copy of the license agreement can be found on the DVD-ROM under the 
# licenses folder.
# If you do not accept the terms of the license agreement you will not be
# allowed to continue with this installation.
# If you accept the terms of the license agreement change the value to true
MKS_LICENSE_AGREEMENT=true

# Java Virtual Machine to be used for the installed product.
# Note: Please provide a path till java.exe (on windows) or java (on unix)
# E.g. C:\\Java\\jre1.8.0_131\\bin\\java.exe or  /usr/bin/java
MKS_JAVA_EXECUTABLE=/opt/pdm/java/bin

# Property USER_INSTALL_DIR used to specify where to install Windchill RV&S Server
# You can use $/$ to specify file separator
# Examples:
# "C:\\Windchill\\RVSServer"
# "/usr/local/Windchill/RVSServer"
# NOTE! Space characters in your paths should be escaped with the backslash 
# character as shown in the following example (for the "Program Files" folder):
# C:\\Program\ Files\\Windchill\\RVSServer
#
USER_INSTALL_DIR=/opt/pdm/Integrity

# Installation Type
# Set MKS_UPGRADE=true if this is installation is part of an upgrade
# of an existing server.
MKS_UPGRADE=true

# Existing Server Installation Directory
# If MKS_UPGRADE=true then set this property to the installation location
# of the existing server installation that is being upgraded.
#
# You can use $/$ to specify file separator
# Examples:
# "C:\\Windchill\\RVSServer"
# "/usr/local/Windchill/RVSServer"
# NOTE! Space characters in your paths should be escaped with the backslash 
# character as shown in the following example (for the "Program Files" folder):
# C:\\Program\ Files\\Windchill\\RVSServer
#
# WARNING: The RCS repository is no longer supported for Source. The repository 
# should be migrated into the database prior to upgrading.
# Contact PTC - Windchill RV&S Support for assistance.
MKS_EXISTING_INSTALL_DIR=/opt/pdm/Integrity-11-2

# Windchill RV&S Server license file location
# Please provide the directory or a full path to the FLEXnet Licensing Module 
# license file (generally license.dat)
# This setting is mandatory.  Without a valid license file, the Windchill RV&S
# Server will not start up.
# 
# The installation program will copy the file from this named location to
# $USER_INSTALL_DIR$/data/license/license.dat
MKS_LICENSE_FILE=/home/ec2-user/license_MKS_15722925.dat

# Specify a database to use with the Windchill RV&S Server
# Valid option values are 'MSSQL', 'ORACLE' or 'EMBEDDED' where:
#   Microsoft SQL Server = MSSQL
#       Oracle Database = ORACLE
#       Embedded Database = EMBEDDED
#
# The embedded database is available for use in configurations where
# Windchill RV&S Server is used only as a FSA Proxy Server with no local
# workflows, documents or configuration management data.
#
# Setting is case sensitive.
MKS_DATABASE_TYPE=ORACLE

#Supported Database Version- 
#For Oracle, Oracle_19c is supported.
#For SQL, MSSQL_2016_SP2+/MSSQL_2017/MSSQL_2019 are supported.
MKS_DATABASE_VERSION=Oracle_19c

#
# If both your previous installation of Windchill RV&S Server and the new one are 
# using the embedded database and you want to copy over the data from the 
# previous installation, set MKS_OLD_INSTALL_LOCATION to the path where the old 
# server is installed
# (eg: MKS_OLD_INSTALL_LOCATION=C:\\Program Files\\Windchill\\RVSServer).
# 
# Note that this must be an embedded database from the Windchill RV&S Server
# 2009 release and not a Pointbase embedded database used in releases prior
# to the 2009 release. Also note that the embedded database must have a backup.
#

# MKS_OLD_INSTALL_LOCATION=

# Database connection parameters
# Parameters are not required if using the EMBEDDED DB
#
# Database server host name
MKS_DATABASE_HOST_NAME=<database name>
# Database server port
# Default for Oracle: 1521
# Default for Microsoft SQL Server: 1433
MKS_DATABASE_PORT=1521
# Database name (for Oracle the Database Service Name)

MKS_DATABASE_NAME=<database name>

# Database login name
MKS_DATABASE_LOGIN_NAME=<database user>
# Database password
# Please note that the password you provide is in plain text, and does not
# get encrypted.
MKS_DATABASE_LOGIN_PASSWORD=<database password>
# Database instance name (only used with Microsoft SQL Server)
# optional, set to the database instance name, if not the default
MKS_INSTANCE_NAME=<instance name>

# Migrate existing data
# If an existing set of Windchill RV&S Server data is detected during
# installation, do you want to migrate this data?
# Setting this value to false and data exists, will cancel the installation.
MKS_MIGRATE_EXISTING_DATA=true

# Do you want to create case-sensitive database tables?  If not, the 
# tables will be case-insensitive. This setting is valid only for new installations.
# To create case-sensitive database tables set MKS_CASE_INSENSITIVE=false
# otherwise, set to true
MKS_CASE_INSENSITIVE=true

# The Windchill RV&S Server has the capability to send emails using triggers.
# The Mail Server and Mail Server properties must be correctly configured in
# order to receive emails.  The Email To property can be a single email address
# or a comma-delimited list of email addresses to deliver notifications to.
# Additional properties can be configured using the Windchill RV&S Administration
# client.
MKS_NOTIFY_MAIL_SERVER=
MKS_NOTIFY_MAIL_SERVER_PORT=
MKS_NOTIFY_EMAIL_TO=

# The Windchill RV&S Server has the capability to run as a Full Server or as a Proxy
# Server.  The Proxy Server can work for Workflows and Documents, Configuration Management
# or both. Post installation, a Full Server can be also run as a Proxy Server at the same time;
# See documentation for details.
# The default value of MKS_PROXY_SERVER is false which means that the installation will be for
# a full server.
MKS_PROXY_SERVER=false

# Workflows and Documents capabilities of the Windchill RV&S Server.  Previously known as Integrity.
# The default value for MKS_WORKFLOWS_AND_DOCUMENTS_ENABLED is true.
MKS_WORKFLOWS_AND_DOCUMENTS_ENABLED=true

# Configuration Management capabilities of the Windchill RV&S Server.  Previously known as Source.
# The default value for MKS_CONFIGURATION_MANAGEMENT_ENABLED is true.
MKS_CONFIGURATION_MANAGEMENT_ENABLED=true

# Product Language
# Choose one language to enable using the two-letter ISO-639 code and optional
# ISO-3166 country code.
# For example:
#      English = en
#          Japanese = ja
#          German = de
#          Chinese Simplified = zh_CN
#          Korean = ko
#          French = fr
# Leaving this blank or specifying an invalid ISO-639 code will
# result in using the default system language.
# If supported, then Windchill RV&S will be presented in that language.
# Otherwise Windchill RV&S will be presented in English.
# In all cases, formatting will be according to this setting.
# For example setting this property to ru would result in using Windchill RV&S in 
# English but with Russian formatting for dates, currency, etc.
#
# The product language must match the silent installer language.
# If this is a new server and you require a different supported product
# language, the silent installer must be run in that language.
# If this is an upgrade of an existing server, the silent installer must be run
# in the same language as the existing server's product language.
MKS_PRODUCT_LANGUAGE=en
```
