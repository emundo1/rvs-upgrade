---
title: Definitions
slug: /Definitions
---
### Server Chain
A server chain is a set of servers linked together. A standard server chain format is separate servers `DEV`, `QA`, `Production`. Thees servers act a pipeline for development. Changes can only be made on the lowest ring on the chain. Changes then need to be promoted up the chain. In the `DEV`, `QA`, `Production` example, any change would first need to be made on the `DEV` server. When that change is validated, it can then be promoted to the `QA` server using the migration wizard on the `DEV` servers administration interface. After promotion, the changes will be on the `QA` server and the `DEV` server. To get the changes on the `Production` server, you will need to promote the changes from the `QA` server using the migration wizard on the `QA` servers administration interface. The changes will now be on all three servers in the chain. 