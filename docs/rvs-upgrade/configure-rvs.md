---
title: Configure RV&S
slug: /Configure-RVS
---

### Checklist Purpose
This ordered list is to outline the configuration process of RV&S 12.4. Thees configurations will set up the proper logging emails, Server hostnames, staging configuration, and gateway configuration. 


### Admin Client Configurations

1. If this server is not part of the production chain the RV&S server might start with a administrator lock that will need to be released. To do this navigate to the administrator client and right click the `workflows and documents` section. A menu will appear select `release admin lock...` and follow the instructions. 

  ![](/img/rvs_upgrade/config/img001.png)

1. The Integrity Server will send e-mail messages based on certain critical situations such as “running out of java heap space”. The message indicates the server name in the e-mail from information as well as the name of the server in the e-mail subject line. The newly restored database came from a different physical server. This requires a reset of these properties in the Integrity Administration Client. Some property changes require a restart of the Integrity Server before they take effect. In this case, no restart is required and the effect is im/imgte. The properties that should be adjusted are:

  **mksis.logging.email.from**

  ![](/img/rvs_upgrade/rvs_upgrade/image002.png)

  **mksis.logging.email.subject**

  ![](/img/rvs_upgrade/rvs_upgrade/image003.png)

  **mksis.im.notifierAddress**

  ![](/img/rvs_upgrade/rvs_upgrade/image004.png)

### Server Level Configuration

1. If the upgrade is creating a new Integrity Server based on a file system from another Integrity Server. If so, the host name in the new Integrity Server file system should reset. This is found in `is.properties` with a key of `mksis.hostname`

    #### ***is.properties***
    ```yaml
    #
    # Hostname to use instead of askng the server.
    #
    mksis.hostname=ilm.devsite.com
    ```

2. Install the latest CPS (Critical Patch Set). You should have already downloaded this during the Prep-Upgrade portion of this checklist, but if not, this is available on the PTC product download site. The installation is the same as hotfixes under previous versions of Integrity with the exception that one install may load many hotfixes. These are cumulative so if more than one CPS is available, only download and install the latest. As an example, assuming that you have downloaded a CPS, look inside it and find the inner zip file.

  Place the inner zip file (example is HF-S170000000-901.zip) in the server /tmp directory.
  change directory to the 11.2 server bin directory and execute the following command using the name of the zip file mentioned above. Shutdown the Integrity Server if it is running.

    ```sh
    cd /opt/pdm/Integrity/bin
    dzdo ./PatchServer /tmp/staging/HF-S170000000-902.zip
    ```

3. Gateway imports and exports on each Integrity server have a file that indicates where to find resources such as Word documents and parsers. The file is called gateway-tools-configuration.xml and it is found on the new Integrity 11.2 server in the directory

  #### ***/opt/pdm/Integrity/data/gateway***
  Scan the files for URLs that specify a different server than the one that you are working with. Replace any that do not match. For example, you may need to replace <https://ilm.devsite.com:443> with <https://ilm2.devsite.com:443>. A effective way to do this is using string replacement in with the `sed` command. use the following command replacing `file` with the file name.

    ```
    sed -i 's/ilm.devsite.com:443/ilm2.devsite.com:443/ FILE
    ```

    After entering this command google how to exit vim :)

  :::caution

  Please remember to replace thees placeholder hostname with the hostname for the specific server.

  :::

4. If the server is part of staging configuration, the next actions depend on where the upgraded server is in the staging configuration chain. If this is a lower level server (e.g. QA or Development) the DBA should have followed the steps described in the Oracle Database section of Prep-Upgrade. If this is a lower stage in the configuration (e.g. QA) it should have been based on a restore of the upgraded production database. If the servers have been previous participants in a staging chain, the task is complete after the execution of “isutil –c migrateServerConfig”. Log into the Administration Client for the Development server and removing the Admin Lock that would have come from the copy of the production database. Because part of the upgrade plan includes creation of new staging configurations including between sandbox servers, there are addition steps for these new participants to a staging configuration.

- Set the following properties in the Administration client for each server.

    ```
    mksis.im.allowPartialAdminMigration = true
    mksis.im.allowAdminDeletionWithAdminMigration = true
    mksis.im.allowPrincipalCreationOnLockedServer = true
    ```

- In the `is.properties` file, set the following properties depending on the stage level: On the Production Server or last server in chain set the following:

    #### ***/opt/pdm/Integrity/config/properties/is.properties***
    ```
    mksis.adminStagingServer=false
    ```

  On the non-production staged servers - e.g. QA or DEV set the following values

    #### ***/opt/pdm/Integrity/config/properties/is.properties***
    ```
    mksis.adminStagingServer=true
    mksis.adminStagingServerDisplayName=server name
    ```
 
  For the above property (DisplayName) server name is the name of the server where the is.properties file is located, not the name of the next server in the chain. E.G. on QA, the value is:
  
    #### ***/opt/pdm/Integrity/config/properties/is.properties***
    ```
    mksis.adminStagingServerDisplayName=ilm.devsite.ga:443 (QA)
    ```

- Use the Administration Client for the servers that subject to locking using the Workflows and Documents node option for obtaining a lock. The lock is to the next lower server in the staging configuration.

5. It is necessary to clear the RV&S database cache to ensure that no ILM 11.2 data is present after the upgrade. To do this run the following command from your workstation. 

  ```
  si diag --hostname=ilm.devsite.ga.com --diag=setCacheRestartAction --target=server full
  ```
- The command will produce the following output. Follow the prompts and after the command completes restart the RV&S server. 

  > Caution!
  > 
  > This command should only be issued on rare occasions as it will force the
  > removal of cached data during the next server restart.
  > 
  > The removal of cached data cascades from the target to all downstream caches.
  > This will negatively impact performance for a period of time.
  > 
  > Would you like to proceed? \[yn](n): y
  > 
  > Please supply a reason for cache reset: upgrade to 12.4
  > 
  > SIRootCache:  Bulkdata, Metadata and supporting structures will be reset on next restart, as requested by user. For reason: "upgrade to 12.4"

- To restart the server run this command from the RV&S bin directory (usually located at (`/opt/pdm/Integrity/bin`) 

  ```
  dzdo ./mksis stop; sleep 3; dzdo ./mksis start
  ```

- Alternatively you can run this command to read the logs as the server restarts. 

  ```
  dzdo ./mksis stop; sleep 3; dzdo ./mksis start; tail -f ../log/server.log
  ```
