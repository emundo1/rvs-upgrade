---
title: Database Refresh
slug: /db-refresh
---

### Checklist Purpose

The numbered checklist that follows serves as a detailed guide for the
refresh of Integrity Databases. The most common use case is a new ILM
release is migrated to production and the QA, Dev, and Training
databases need to match the new ILM release.

### Ticket Request for Oracle Database Support 

:::caution
For purposes of this documentation we will use dummy data you will want to consult your ticket for the correct database names.
:::
1. Identify the source databases. Normally the source database will be the production Integrity database.

  | Integrity Instance | Purpose    | Database Server      | Database Name | DB Owner |
  |:--------------------:|:------------:|:----------------------:|:---------------:|:----------:|
  | ilm.devsite.com    | Production | database.devsite.com | ilmprod       | ilm      |

2. Identify the target databases. These are the databases to be
refreshed using the data from the source identified in step 1 above.
In many cases, not all databases will need to be refreshed depending
on the on-going activity on each server, so choose carefully which
servers to refresh.

  | Integrity Instance   | Purpose           | Database Server       | Database Name | DB Owner |
  |:----------------------:|:-------------------:|:-----------------------:|:---------------:|:----------:|
  | ilm-dev.devsite.com  | Dev Server        | database.devsite.com  | ilmdev        | ilm-dev  |
  | ilm-qa.devsite.com   | QA Server         | database.devsite.com  | ilmqa         | ilm-qa   |
  | ilm2-dev.devsite.com | Team 2 Dev Server | database2.devsite.com | ilm2dev       | ilm2-dev |
  | ilm2-qa.devsite.com  | Team 2 QA Server  | database2.devsite.com | ilm2QA        | ilm2-qa  |

3. Create a ticket that specifies the source and target databases.
In addition to specifying the date and time for the refresh, the
ticket should include a request to grant the DB Owner of each
database the following:
   1. Grant the DB Owner the "CTXAPP" role,
   2. Grant SELECT privileges on the view CTXSYS.CTX_INDEXES.
   3. To do this ask the DBA to apply the following statements to each target database (Where ilm-dev is replace by the database owner):
   	
			grant CTXAPP to ilm-dev;
			grant SELECT on CTXSYS.CTX_INDEXES to ilm-dev;


### Prior to Start of Database Refresh

1.  Verify that there are no administrative changes in progress on the
	source server.

2.  Notify users of target servers of planned downtime (could be 4 to 5
	hours).

3.  Stop the target Integrity Servers. There is no need to stop the
	source Integrity Server

4.  Get the “as of” time for the backup on the source server.

### While the DBA is Performing the Refresh


1.  Make file system backups of critical directories on the target
	servers and place the backups in the installer’s home directory on
	each server. Directories listed below are required to be in-synch in
	order to lock the staging server configurations. The directories
	are:
    - /opt/pdm/Integrity/data/public_html
    - /opt/pdm/Integrity/data/reports
    - /opt/pdm/Integrity/data/triggers
2. To achive this you can run the following commands on the destination server. 

  		mkdir datadir
        rsync -rav -e ssh user@ilm.devsite.com:/opt/pdm/Integrity/data/{public_html,reports,triggers} /home/user/datadir/

    - `mkdir datadir` Will create a directory called datadir in this case this was run in our users home directory `/home/user/datadir/`
    - `rsync -rav -e ssh user@ilm.devsite.com:/opt/pdm/Integrity/data/{public_html,reports,triggers} /home/user/datadir/` will download our files from the source server to the destination server.
    	+ `rsync` will sync files from one server to another
    	+ `-rav -e ssh` are command line arguments that will augment how our sync is ran.
    		* `r` will download files recessively.
    		* `a` will run the sync in archival mode to not waste time downloading files we already have.
    		* `v` will print out verbose logs. 
    		* `e ssh` will use SSH as its transfer protocol.
    	+  `user@ilm.devsite.com:/opt/pdm/Integrity/data/{public_html,reports,triggers}` will download `public_html`, `reports`, `triggers` as the `user` user from `ilm.devsite.com`
    		* make sure to swap out `user` for your unix user and `ilm.devsite.com` for the correct source server. 
    	+  `/home/user/datadir/` will save the files on the destination servers at the directory we created in the last command. 

### After the DBA Refresh is Complete

1.  Copy the directories saved in the previous step from the installer’s
	home directory to /opt/pdm/Integrity/data.
    1. Assuming that RV&S was installed at `/opt/pdm/Integrity` remove the reports, public_html, and triggers directories.
    
        ```sh 
        dzdo rm -rfv /opt/pdm/Integrity/data/{public_html,triggers,reports}
        ```
    
    2. Then move the files from your temporary directory into the Integrity install directory.  
        :::caution 

        Remember to change the user directory to match your user.

        :::

        ```
        dzdo cp –a /home/user/datadir/{reports,public_html,triggers} /opt/pdm/Integrity/data/
        ```

2.  Start the QA and Dev servers and verify in the server.log files that
	QA establishes a lock on Prod and Dev establishes a lock on QA.

3.  For other servers that are not part of the Dev-QA-Prod staging
	chain, any desired locks that are to be maintained should be handled
	as follows:

	a.  Start the last server in the chain (equivalent of production).
		It will show as locked to QA. Break that lock using the
		administration client.

	b.  Start the next server in the chain and verify that the last
		server in the chain has a lock to correct server now. Verify
		that the server that was just started is not locked by a
		incorrect server. If it does have a lock to a server from
		another staging chain, then break that lock.

	c.  Start the first server in the chain and verify that the middle
		server has a lock from the first server.

### After Starting the Integrity Servers

1.  Execute the database statistics collection script as shown below for each Integrity Server

		im diag --diag=computestatistics --param="Issues" --hostname=XXXXXX
2.  Correct the server name. Normally, the value to replace is the name of the source Integrity Server.

		im diag --diag=runsql --param="update DBFile set Contents = replace(Contents, 'https://ilm.devsite.com:443', 'https://XXXXX.devsite.com:443')" --hostname=XXXXX
4.  Ensure that the Integrity Users group on the server only include groups that are allowed to log on that server. The ACL aa.mks should be checked to verify that everyone is cleared for login allowed.
5.  Verify that only users of the Integrity Users group has login allowed.
6.  Update the values for sprint and database refresh date in the MKS Solution item. This will be item 1 on all Servers.
7.  Verify that the database users have been granted 
	1. The `CTXAPP` role for the database user.
	2. Select privileges on the view `CTXSYS.CTX_INDEXES` for the database user.
	3. The specific statements to be executed on each target database are:

			grant CTXAPP to ilm-dev;
			grant SELECT on CTXSYS.CTX_INDEXES to ilm-dev;
	
	4. Where ilm-dev is replace by the DB Owner for that specific database. 

7.  The Integrity Server will send e-mail messages based on certain
	critical situations such as “running out of java heap space”. The
	message provides the server name in the e-mail as
	well as the hostname in the e-mail subject line. The newly
	restored database came from a different physical server. This
	requires a reset of these properties in the Integrity Administration
	Client. Some property changes require a restart of the Integrity
	Server before they take effect. In this case, no restart is required
	and the effect is immediate. The properties that should be adjusted
	are:
    1. **mksis.logging.email.from**
		
		![](/img/rvs_upgrade/db_refresh/image001.png)
	
    2. **mksis.logging.email.subject**
		
		![](/img/rvs_upgrade/db_refresh/image002.png)
	
    3. **mksis.im.notifierAddress**
		
		![](/img/rvs_upgrade/db_refresh/image003.png)
