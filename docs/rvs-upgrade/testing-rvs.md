---
title: Testing RV&S
slug: /testing-rvs
---
### Checklist Purpose

The numbered checklist that follows is meant to outline the testing process for RV&S. This document will cover the following:

- **Creating a GA Requirements Document**
- **Creating a Test Procedure Document**
- **Tracing Test Steps to GA Requirements**
- **Creating a Test Session**

It will be helpful to follow along while reading through this document. To follow along you will need a project that is safe to use as a sandbox. You should ask you're manager what project will be used for testing. 

### Create A Requirements Document

1. The first step in this process is to create a `GA Requirements Document`. This document will house Requirements that will be verified by Test Steps created later on in this document. To create a Requirements Document you will need to select the documents menu item at the top navigation bar in RV&S. From that context menu you will want to select the create document item. This will open a menu with a list of documents available for creation. 
    - Give short title. 
    - Select the project. 
- Create requirements.
    - Title the page with a heading of size 1
    - Ctr arrow right to indent. 
    - Make sure you are using the correct requirements in the category tab. 
- Create a test procedure document. 
    - Create a GA test procedure document. 
    - Select the correct project. 
    - Give it a short title.
    - Give it a heading title. 
    - step configuration. -> this is prep before the step execution. 
    - step preparation. -> this is also prep work before the step execution. 
    - then create step executions. -> this is the test to be run. 
- Traceing the document. 
    - Have the docs side by side. 
    - select the requirement. 
    - press the alt key and drag the requierment to the test step that verifies it. 
- remove suspect flag. 
    - go to content > suspect > remove suspect flag.
- create a test session.
    - create a GA test session. 
    - give it a short title. 
    - select a project. 
    - select the test revision. 
    - Give it a assigned user. 
    - give it a start date. 
    - Use sit for unit testing. 
    - and the location poway. 
    - copy the test procedure item ID and add it to the test session under tests. 
    - in the configuration tab set up the asst configuration. 
    - put the test session into in testing. 