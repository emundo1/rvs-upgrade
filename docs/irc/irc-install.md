---
title: IRC Install Guide
slug: /irc-install
---

### Introduction
Integrity Requirements Connector (IRC) is a platform that supports the synchronization of DOORS modules with Integrity Documents.  IRC runs as a plug-in on the open source Eclipse IDE platform. 

### Installing JAVA

To install IRC, You will first need to install JAVA. You can use this link to download the latest version of JAVA:

**[Download Link For Java](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)**

Download the 32-bit version of JAVA for windows and install it following the prompts. 

### Locating the IRC Installer

You will want to follow this link to get the latest version of Integrity Requirements Connector (IRC): 

**[Link To Download](https://iqnoxllc.sharepoint.com/:u:/s/externalSharing/EcgHH-2jV_RMpyoPd6GHVawBeiERVGjxmW4YoIxOCkP_iw?e=1Xy95O)**

### Installing Integrity Requirements Connector

To Install IRC you need to run the installer you just downloaded as an administrator. This will begin the installation process. Follow the prompts on your screen to complete the install. The default settings will do for this install. 


### Setting Up The IRC Workspace.

After installing Integrity Requirements Connector, the product will need to be configured. Open Integrity Requirements Connector. You should be able to find a shortcut on your desktop. If not you will be able to find the program in your start menu. When you first open Integrity Requirements Connector, you will be prompted for a workspace location. If there is currently not a workspace at the location that you choose, one will be created for you. Specify `C:\irc_workspace`.  This workspace will be used by our automation scripts.

![](/img/irc/img004.png)

### Eclipse Preferences

After launching IRC using the workspace `c:\irc_workspace`, the preferences can be configured. The preferences provide the rules for how IRC will communicate with DOORS and with Integrity. Preferences are accessible from the top menu `Windows > Preferences`. After opening the Preference Wizard, expand the PTC Integrity Requirements Connector node as shown below. We will update the following nodes: 

- **License Configuration**
- **Roundtrip configuration** 
- **Login Configuration** 
- **User Profile**

Other nodes may require update for users who are creating new synchronizations, but are not needed for configuring the automation.

![](/img/irc/img003.png)

### License Configuration

After clicking on the License Configuration node, select `New server ...` to enter the Licensing server to allow IRC to locate the license file.  Click Apply after entering the license path to save the new license path. Click the link below to locate the correct hostname and port for our licensing server.

**[License Server Link](#)**

![](/img/irc/img001.png)

![](/img/irc/img002.png)

### Roundtrip Configuration

Open the Roundtrip Configuration node and verify that `Show difference view before update operation` is unchecked. If this option is checked, the automation will not run to completion because there is a GUI response that will be required. 

:::note

When problems occur during a synchronization, this is a useful tool when manually executing a synchronization.

:::

![](/img/irc/img005.png)

### Login Configuration

We will need to configure two login configurations, one for the **DOORS production server** and one for the **Integrity Production Server**. Select the Login Configuration node from the Preferences wizard and then click the new button to add each additional server. First we will add the DOORS server. Enter the information below, replacing the User name value with the one that you will use to login to DOORS.

![](/img/irc/img006.png)
![](/img/irc/img007.png)

Next add a new login configuration for the Integrity production server, replacing Username with the user that you will be using to login to Integrity. The Trust store `jssecacerts` can be filled in automatically using the `Browse ...` button. The `jssecacerts` file can be found in your Integrity client directory at the following path:

```
C:\Program Files (x86)\Integrity\ILMClient11\jre\lib\security\jssecacerts
```

![](/img/irc/img008.png)


### User Profile

The User Profile tab will allow your information to automatically be picked up by Integrity Requirements Connector.

![](/img/irc/img009.png)


### Conclusion

With thees steps completed you now have installed and configured the Integrity Requirements Connector You should now look at our guide on installing and preforming Integrity Requirements Connector syncs using our custom automations.
