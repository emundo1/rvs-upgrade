---
title: Docusaurus
slug: /docusaurus-info
---


### Introduction

Docusaurus is a tool designed to make it easy for teams to publish documentation websites without having to worry about the infrastructure and design details. At its core, all a user has to provide are documentation files written in markdown, customization of a provided home page written in React, and a few configuration modifications. Docusaurus handles the rest by providing default styles, site formatting, and simple document navigation.


### Docusaurus & GitLab

Docusaurus outputs are single statically generated meaning that they can be hosted using GitLab's pages feature. Hosting a page on GitLab pages is light weight and straight forward. The steps are outlined below.

1. Make a change to the GitLab project. 
2. Request a merge request to the master branch.
3. When your change is merged and approved GitLabs CI/CD pipeline takes over. 
4. When the Pipeline completes successfully the changes will automatically be deployed to GitLab pages.

Below you can see a crude diagram of what happens when the CI/CD pipeline takes control. 

![](/img/docusaurus-info/img001.png)

### Benefits

- ✅ Traceability of changes and version history.
- ✅ Change approval process. 
- ✅ User access management. (Only users with access to the repository can access the document)

- ✂️ **Developer experience**
  - Multiple bootstrapping templates to get your site up and running, start writing your docs right now
  - Universal configuration entry point to make it more maintainable by contributors
  - Hot reloading with lightning fast incremental build on changes
  - Route-based code and data splitting
  - Publish to GitHub Pages, Netlify, and other deployment services with ease

Docusaurus 2 is born to be compassionately accessible to all your users, and lightning fast.

- ⚡️ **Lightning fast** - Docusaurus 2 follows the [PRPL Pattern](https://developers.google.com/web/fundamentals/performance/prpl-pattern/) that makes sure your content loads blazing fast
- 🦖 **Accessible** - Attention to accessibility, making your site equally accessible to all users


### Project structure

Assuming you chose the classic template and named your site `my-website`, you will see the following files generated under a new directory `my-website/`:

```sh
my-website
├── blog
│   ├── 2019-05-28-hola.md
│   ├── 2019-05-29-hello-world.md
│   └── 2020-05-30-welcome.md
├── docs
│   ├── doc1.md
│   ├── doc2.md
│   ├── doc3.md
│   └── mdx.md
├── src
│   ├── css
│   │   └── custom.css
│   └── pages
│       ├── styles.module.css
│       └── index.js
├── static
│   └── img
├── docusaurus.config.js
├── package.json
├── README.md
├── sidebars.js
└── yarn.lock
```

### Working with Office files.

Docusaurus can't render Microsoft office file natively this is what we need share point for. We can link to files on a share point directory. Any user with privileges to view that file will be able to access that file will then be able to view it. Here are some examples.  

- **[Link to Excel File](https://iqnoxllc.sharepoint.com/:x:/s/externalSharing/EcOr4oD0JmRNiHGY-dCvkhMBsVs2O7zfv8Z3iL7mzm7pBA?e=37gbl5)**
- **[Link to Word File](https://iqnoxllc.sharepoint.com/:w:/s/externalSharing/Eb9DuesHM6pIq11UOpeKaUkBZCuMfW9IV3zwltN1gAjJtQ?e=lYhIJt)**
- **[Link to Installer file](https://iqnoxllc.sharepoint.com/:u:/s/externalSharing/EcgHH-2jV_RMpyoPd6GHVawBeiERVGjxmW4YoIxOCkP_iw?e=lvTn6S)**
